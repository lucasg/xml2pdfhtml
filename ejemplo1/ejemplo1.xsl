<?xml version="1.0" encoding="UTF-8"?>
<!-- definicion del namespace -->
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="teiCorpus/TEI/text/group">
    <html>
      <body>
        <h2>EJEMPLO 1</h2>
        <p><xsl:value-of select="text/body/div/." /></p>
        <!-- primera tabla -->
        <h2>EJEMPLO 2</h2>
        <table>
          <thead>
            <tr bgcolor="green">
              <th>Lenguaje</th>
              <th>Codigo</th>
            </tr>
          </thead>
          <tbody>
            <!-- EJEMPLO 2 -->
            <xsl:for-each select="text">
              <tr>
                <td><xsl:value-of select="body/div[last()-1]"/></td>
                <td><xsl:value-of select="body/div[last()]"/></td>
              </tr>
            </xsl:for-each>
          </tbody>
        </table>
        <!-- segunda tabla -->
        <h2>EJEMPLO 3</h2>
        <table>
          <thead>
            <tr bgcolor="#5aaed4">
              <th>Lenguaje</th>
              <th>Codigo</th>
            </tr>
          </thead>
          <tbody>
            <!-- EJEMPLO 3 -->
            <xsl:for-each select="text">
              <xsl:if test="body[@id='python']">
              <tr>
                <td><xsl:value-of select="body/div[@type='lang']"/></td>
                <td><xsl:value-of select="body/div[@type='code']"/></td>
              </tr>
              </xsl:if>
            </xsl:for-each>
          </tbody>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>