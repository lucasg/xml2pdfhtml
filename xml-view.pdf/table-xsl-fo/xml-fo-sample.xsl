<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
  <xsl:template match="/">
    <fo:root>
      <fo:layout-master-set>
        <fo:simple-page-master master-name="simple" page-height="29.7cm" page-width="21cm" margin-top="1cm" margin-bottom="2cm" margin-left="2cm" margin-right="2cm">
          <fo:region-body margin-top="3cm" />
        </fo:simple-page-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="simple">
        <fo:flow flow-name="xsl-region-body">
          <xsl:apply-templates select="teiCorpus" />
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
  <xsl:template match="teiCorpus">
    <fo:block>
      <xsl:apply-templates select="TEI" />
    </fo:block>
  </xsl:template>
  <xsl:template match="TEI/text/group">
    <fo:block>
      <fo:table table-layout="fixed" width="100%" background-color="white" line-height="16pt" space-before.optimum="10pt" space-after.optimum="6pt" text-align="left">
        <fo:table-column column-width="25%" />
        <fo:table-column column-width="25%" />
        <fo:table-column column-width="25%" />
        <fo:table-column column-width="25%" />
        <fo:table-header background-color="#5aaed4">
          <fo:table-row>
            <fo:table-cell>
              <fo:block color="#039" font-size="10pt">IPA</fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block color="#039" font-size="10pt">TNR</fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block color="#039" font-size="10pt">MORPH</fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block color="#039" font-size="10pt">GLOSS</fo:block>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-header>
        <xsl:for-each select="text">
          <fo:table-body>
            <fo:table-row>
              <fo:table-cell border-bottom-width="0.09mm" border-bottom-color="#c0c0c0" border-bottom-style="groove" color="#669">
                <fo:block padding-bottom="1mm" padding-top="1mm">
                  <xsl:value-of select="body/div[@type='IPA']" />
                </fo:block>
              </fo:table-cell>
              <fo:table-cell border-bottom-width="0.09mm" border-bottom-color="#c0c0c0" border-bottom-style="groove" color="#669">
                <fo:block padding-bottom="1mm" padding-top="1mm">
                  <xsl:value-of select="body/div[@type='TNR']" />
                </fo:block>
              </fo:table-cell>
              <fo:table-cell border-bottom-width="0.09mm" border-bottom-color="#c0c0c0" border-bottom-style="groove" color="#669">
                <fo:block padding-bottom="1mm" padding-top="1mm">
                  <xsl:value-of select="body/div[@type='MORPH']" />
                </fo:block>
              </fo:table-cell>
              <fo:table-cell border-bottom-width="0.09mm" border-bottom-color="#c0c0c0" border-bottom-style="groove" color="#669">
                <fo:block padding-bottom="1mm" padding-top="1mm">
                  <xsl:value-of select="body/div[@type='GLOSS']" />
                </fo:block>
              </fo:table-cell>
            </fo:table-row>
          </fo:table-body>
        </xsl:for-each>
      </fo:table>
    </fo:block>
  </xsl:template>
</xsl:stylesheet>