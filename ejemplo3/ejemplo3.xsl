<?xml version="1.0" encoding="UTF-8"?>
<!-- definicion de namespaces -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">
  <xsl:template match="/">
    <fo:root>

      <fo:layout-master-set>
        <fo:simple-page-master master-name="pdf">
          <fo:region-body margin-top="1cm" />
        </fo:simple-page-master>
      </fo:layout-master-set>

      <fo:page-sequence master-reference="pdf">
        <fo:flow flow-name="xsl-region-body">
          <!-- comienzo de template-->
          <xsl:apply-templates select="im" />
        </fo:flow>
      </fo:page-sequence>

    </fo:root>
  </xsl:template>
  <!-- definicion de template -->
  <xsl:template match="im">
    <fo:block>
      <fo:table table-layout="fixed" width="100%">
        <!-- definicion de la cantidad de columnas de la tabla -->
        <fo:table-column column-width="50%" />
        <fo:table-column column-width="50%" />

        <fo:table-header background-color="gray">
          <fo:table-row>
            <fo:table-cell>
              <fo:block>Lenguaje</fo:block>
            </fo:table-cell>
            <fo:table-cell>
              <fo:block>Codigo</fo:block>
            </fo:table-cell>
          </fo:table-row>
        </fo:table-header>

        <xsl:for-each select="programming/in">
          <fo:table-body>
            <fo:table-row>
              <fo:table-cell>
                <fo:block>
                  <xsl:value-of select="lang" />
                </fo:block>
              </fo:table-cell>
              <fo:table-cell>
                <fo:block>
                  <xsl:value-of select="code" />
                </fo:block>
              </fo:table-cell>
            </fo:table-row>
          </fo:table-body>
        </xsl:for-each>

      </fo:table>
    </fo:block>
  </xsl:template>
</xsl:stylesheet>