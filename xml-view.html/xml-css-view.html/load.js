function loadXMLDoc(filename){
  if (window.ActiveXObject){
    xhttp = new ActiveXObject("Msxml2.XMLHTTP");
  }
  else{
    xhttp = new XMLHttpRequest();
  }
  xhttp.open("GET", filename, false);
  try {xhttp.responseType = "msxml-document"} catch(err) {} //IE
  xhttp.send("");
  return xhttp.responseXML;
}

function displayResult(){
  xml = loadXMLDoc("xml-sample.xml");
  xsl = loadXMLDoc("xsl-sample.xsl");
  // code for IE
  if (window.ActiveXObject || xhttp.responseType == "msxml-document"){
    ex = xml.transformNode(xsl);
    document.getElementById("body").innerHTML = ex;
  }
  // code for Chrome, Firefox, Opera, etc.
  else if (document.implementation && document.implementation.createDocument){
    xsltProcessor = new XSLTProcessor();
    xsltProcessor.importStylesheet(xsl);
    resultDocument = xsltProcessor.transformToFragment(xml, document);
    document.getElementById("body").appendChild(resultDocument);
  }
}