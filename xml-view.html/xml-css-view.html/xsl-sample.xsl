<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="teiCorpus/TEI/text/group">
    
    <html>
      <body>
        <table id="box-table-a">
          <thead>
            <tr bgcolor="#5aaed4">
              <th scope="col">IPA</th>
              <th scope="col">TNR</th>
              <th scope="col">MORPH</th>
              <th scope="col">GLOSS</th>
            </tr>
          </thead>
          <tbody>
            <xsl:for-each select="text">
              <tr>
                <td><xsl:value-of select="body/div[@type='IPA']"/></td>
                <td><xsl:value-of select="body/div[@type='TNR']"/></td>
                <td><xsl:value-of select="body/div[@type='MORPH']"/></td>
                <td><xsl:value-of select="body/div[@type='GLOSS']"/></td>
              </tr>
            </xsl:for-each>
          </tbody>
        </table>
      </body>
    </html>
    
  </xsl:template>
</xsl:stylesheet>