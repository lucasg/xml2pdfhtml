<?xml version="1.0" encoding="UTF-8"?>
    <xsl:stylesheet version="1.0"
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform">     
    <xsl:template match="teiCorpus/TEI/text/group">

      <html>
      <body>
        <table border="1">
          <tr bgcolor="#5aaed4">
            <th>IPA</th>
            <th>TNR</th>
            <th>MORPH</th>
            <th>GLOSS</th>
          </tr>
          <xsl:for-each select="text">
          <tr>
            <td><xsl:value-of select="body/div[@type='IPA']"/></td>
            <td><xsl:value-of select="body/div[@type='TNR']"/></td>
            <td><xsl:value-of select="body/div[@type='MORPH']"/></td>
            <td><xsl:value-of select="body/div[@type='GLOSS']"/></td>
          </tr>
          </xsl:for-each>
        </table>
      </body>
      </html>

    </xsl:template>
    </xsl:stylesheet>